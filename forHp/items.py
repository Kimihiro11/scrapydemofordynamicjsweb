# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class ForhpItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    stars = scrapy.Field()
    customer_name = scrapy.Field()
    content = scrapy.Field()

    def get_insert_sql(self):
        insert_sql = """
            insert into review_info(stars, customer_name, content)
            VALUES (%s, %s, %s,) ON DUPLICATE KEY UPDATE content=VALUES(content)
        """
        params = (self["stars"], self["customer_name"], self["content"])

        return insert_sql, params