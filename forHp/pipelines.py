# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html
import MySQLdb
from MySQLdb import cursors
from twisted.enterprise import adbapi


class ForhpPipeline(object):
    def process_item(self, item, spider):
        return item


class MysqlPipeline(object):
    #采用同步的机制写入mysql
    def __init__(self):
        self.conn = MySQLdb.connect('127.0.0.1', 'root', 'root', 'spider_demo', charset="utf8", use_unicode=True)
        self.cursor = self.conn.cursor()

    def process_item(self, item, spider):

        insert_sql = """
            insert into review_info(stars, customer_name, content)
            VALUES (%s, %s, %s) ON DUPLICATE KEY UPDATE content=VALUES(content)
        """
        self.cursor.execute(insert_sql, (item["stars"], item["customer_name"], item["content"]))
        self.conn.commit()


