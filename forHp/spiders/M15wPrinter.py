# -*- coding: utf-8 -*-
import json

import scrapy
from scrapy.selector import Selector
from twisted.plugin import pickle

from forHp.items import ForhpItem

class M15wprinterSpider(scrapy.Spider):
    name = 'M15wPrinter'
    allowed_domains = ['https://store.hp.com']
    start_urls = ['https://store.hp.com/us/en/pdp/hp-laserjet-pro-m15w-printer']

    start_json_url = ['https://api.bazaarvoice.com/data/batch.json?passkey=caBZoE5X0dmsywGCMoQmo6OPymWLQFnY37VernuC3SGkY&apiversion=5.5&displaycode=8843-en_us&resource.q0=products&filter.q0=id%3Aeq%3AW2G51A_BVEP_BGJ&stats.q0=reviews&filteredstats.q0=reviews&filter_reviews.q0=contentlocale%3Aeq%3Aen_US&filter_reviewcomments.q0=contentlocale%3Aeq%3Aen_US&resource.q1=reviews&filter.q1=isratingsonly%3Aeq%3Afalse&filter.q1=productid%3Aeq%3AW2G51A_BVEP_BGJ&filter.q1=contentlocale%3Aeq%3Aen_US&sort.q1=rating%3Adesc&stats.q1=reviews&filteredstats.q1=reviews&include.q1=authors%2Cproducts%2Ccomments&filter_reviews.q1=contentlocale%3Aeq%3Aen_US&filter_reviewcomments.q1=contentlocale%3Aeq%3Aen_US&filter_comments.q1=contentlocale%3Aeq%3Aen_US&limit.q1=8&offset.q1=0&limit_comments.q1=3&resource.q2=reviews&filter.q2=productid%3Aeq%3AW2G51A_BVEP_BGJ&filter.q2=contentlocale%3Aeq%3Aen_US&limit.q2=1&resource.q3=reviews&filter.q3=productid%3Aeq%3AW2G51A_BVEP_BGJ&filter.q3=isratingsonly%3Aeq%3Afalse&filter.q3=issyndicated%3Aeq%3Afalse&filter.q3=rating%3Agt%3A3&filter.q3=totalpositivefeedbackcount%3Agte%3A3&filter.q3=contentlocale%3Aeq%3Aen_US&sort.q3=totalpositivefeedbackcount%3Adesc&include.q3=authors%2Creviews%2Cproducts&filter_reviews.q3=contentlocale%3Aeq%3Aen_US&limit.q3=1&resource.q4=reviews&filter.q4=productid%3Aeq%3AW2G51A_BVEP_BGJ&filter.q4=isratingsonly%3Aeq%3Afalse&filter.q4=issyndicated%3Aeq%3Afalse&filter.q4=rating%3Alte%3A3&filter.q4=totalpositivefeedbackcount%3Agte%3A3&filter.q4=contentlocale%3Aeq%3Aen_US&sort.q4=totalpositivefeedbackcount%3Adesc&include.q4=authors%2Creviews%2Cproducts&filter_reviews.q4=contentlocale%3Aeq%3Aen_US&limit.q4=1']

    headers = {
        "Accept": "*/*",
        "Accept-Encoding": "gzip, deflate, br",
        "Accept-Language": "zh-CN,zh;q=0.9",
        "cache-control": "no-cache",
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.10",
        "Referer": 'https://plugin.monotote.com/affiliate.html',
    }

    def parse(self, response):
        rew_json = json.loads(response.body.decode('utf-8'))
        review_infos = rew_json['BatchedResults']['q1']['Results']
        #抓取第一个数据包内三列数据作为示例
        for review_info in review_infos:
            review_item = ForhpItem()
            review_item['stars'] = review_info['Rating']
            review_item['customer_name']  = review_info['UserNickname']
            review_item['content']  = review_info['ReviewText']

            yield review_item


    def start_requests(self):
        from selenium import webdriver
        browser = webdriver.Chrome(executable_path="D:/Python/scrapy/Drivers/chromedriver_win32/chromedriver.exe")
        browser.get("https://store.hp.com/us/en/pdp/hp-laserjet-pro-m15w-printer")
        #网页加载很慢需等待
        browser.find_element_by_css_selector('#initReviews').click()



        return [scrapy.Request(url=self.start_json_url[0], dont_filter=True, headers=self.headers)]

